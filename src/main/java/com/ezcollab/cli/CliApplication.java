package com.ezcollab.cli;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.shell.jline.PromptProvider;


@SpringBootApplication
@PropertySource({"classpath:/message.properties"})
public class CliApplication implements PromptProvider{

	public static void main(String[] args) {
		SpringApplication.run(CliApplication.class, args);
	}


	@Override
	public AttributedString getPrompt() {
		return new AttributedString("uslegalpro:>",
				AttributedStyle.DEFAULT.foreground(AttributedStyle.BLUE));
	}
}
