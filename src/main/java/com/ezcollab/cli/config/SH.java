package com.ezcollab.cli.config;

import com.ezcollab.cli.component.*;
import com.ezcollab.cli.http.RequestUtils;
import com.ezcollab.cli.shell.InputReader;
import com.ezcollab.cli.shell.ShellHelper;
import com.ezcollab.cli.utils.ShellUtils;
import com.ezcollab.cli.utils.TableViewResolver;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SH {

    public static ShellUtils shellUtils;
    public static ShellHelper shellHelper;
    public static FilingCodeService code;
    public static EFSPService efsp;
    public static Environment env;
    public static InputReader inputReader;
    public static RequestUtils requestUtils;
    public static EfileService efileService;
    public static AccountService accountService;
    public static PaymentService paymentService;
    public static ServiceContactService serviceContactService;
    public static AttorneyService attorneyService;
    public static TableViewResolver tableViewResolver;
    public static FirmUserService firmUserService;
    public static TemplateProcess templateProcess;

    SH(ShellUtils shellUtils, ShellHelper shellHelper, FilingCodeService code, EFSPService efsp, Environment env, InputReader inputReader,
       RequestUtils requestUtils, EfileService efileService, AccountService accountService, PaymentService paymentService, AttorneyService attorneyService,
       TableViewResolver tableViewResolver, ServiceContactService serviceContactService, FirmUserService firmUserService, TemplateProcess templateProcess) {
        SH.shellUtils = shellUtils;
        SH.shellHelper = shellHelper;
        SH.code = code;
        SH.efsp = efsp;
        SH.env = env;
        SH.inputReader = inputReader;
        SH.requestUtils = requestUtils;
        SH.accountService = accountService;
        SH.efileService = efileService;
        SH.paymentService = paymentService;
        SH.attorneyService = attorneyService;
        SH.tableViewResolver = tableViewResolver;
        SH.serviceContactService = serviceContactService;
        SH.firmUserService = firmUserService;
        SH.templateProcess = templateProcess;
    }

}
