package com.ezcollab.cli.config;

import com.ezcollab.cli.http.ParamName;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalStorage {

    private static String token = null;
    private static String clientToken = null;
    private static String state = null;
    private static String envelopId = null;
    private static HashMap<String, Object> efile = new HashMap<>();
    private static List<String> file = new ArrayList<>();
    private static List<Map<String, Object>> case_party = new ArrayList<>();
    private static List<Map<String, Object>> cross_reference = new ArrayList<>();
    private static List<Map<String, Object>> service_contact = new ArrayList<>();
    private static List<Map<String, Object>> filing = new ArrayList<>();
    private static Map<String, Map> caseCategoryCode = new HashMap();
    private static Map<String, String> documentType = new HashMap();
    private static Map<String, Map> filingCode = new HashMap();
    private static boolean serviceContactEnable = false;

    private static boolean isIndividual=false;

    public static boolean isServiceContactEnable() {
        return serviceContactEnable;
    }

    public static void setServiceContactEnable(boolean serviceContactEnable) {
        LocalStorage.serviceContactEnable = serviceContactEnable;
    }

    public static boolean isIndividual(){
        return isIndividual;
    }
    public static void setIndividual(boolean isIndividualV){
        isIndividual=isIndividualV;
    }

    public static List<Map<String, Object>> getCaseParty() {
        return case_party;
    }

    public static List<Map<String, Object>> getCrossRef() {
        return cross_reference;
    }

    public static List<Map<String, Object>> getFiling() {
        return filing;
    }

    public static void initDocumentType(String key, String value) {
        documentType.put(key, value);
    }

    public static void clearDocumentType() {
        documentType.clear();
    }

    public static void initCaseCategoryCode(String key, Map value) {
        caseCategoryCode.put(key, value);
    }

    public static void clearCaseCategoryCode() {
        caseCategoryCode.clear();
    }

    public static void initFilingCode(String key, Map value) {
        filingCode.put(key, value);
    }

    public static void clearFilingCode() {
        filingCode.clear();
    }

    public static void initToken(String tokenvalue) {
        token = tokenvalue;
    }

    public static void initEnvelopId(String envelopIdV) {
        envelopId = envelopIdV;
    }

    public static String getEnvelopId() {
        return envelopId;
    }


    public static String token() {
        return token;
    }
    public static String clientToken() {
        return clientToken;
    }

    public static void initClientToken(String tokenvalue) {
        if(StringUtils.isBlank(clientToken)) {
            if(StringUtils.isBlank(tokenvalue)) {
                tokenvalue=SH.shellUtils.singleLine("Enter your client token",null,true);
            }
            clientToken=tokenvalue;
            return;
        }
        clientToken=null;
    }
    public static void initCaseParty(Map<String, Object> map) {
        case_party.add(map);
    }

    public static void initCrossRef(Map<String, Object> map) {
        cross_reference.add(map);
    }

    public static void initFiling(Map<String, Object> map) {
        filing.add(map);
    }

    public static void initEfile(String key, String value) {
        efile.put(key, value);
    }

    public static void initEfile(HashMap map) {
        efile.putAll(map);
    }

    public static String getEfile(String key) {
        return (String) efile.get(key);
    }


    public static Map<String, Object> completeEfile() {
        efile.put(ParamName.CASE_PARTY, case_party);
        Map caseCategory = caseCategoryCode.get(efile.get(ParamName.CASE_CATEGORY));
        if (caseCategory != null && !caseCategory.isEmpty()) {
            efile.put(ParamName.ECF_CASE_TYPE, caseCategory.get("ecf_case_type"));
        }
        if(StringUtils.isBlank((CharSequence) efile.get(ParamName.ATTORNEY))){
            efile.put(ParamName.ATTORNEY,"PRO SE");
        }
        efile.put(ParamName.SERVICE_CONTACT,service_contact);
        efile.put(ParamName.FILING, filing);
        efile.put(ParamName.CROSS_REFERENCE, cross_reference);
        return efile;
    }

    public static Map<String,Map> caseCategoryCode(){
        return caseCategoryCode;
    }

    public static void initFile(String filePath) {
        file.add(filePath);
    }

    public static List<String> getFile() {
        return file;
    }


    public static void initState(String stateV) {
        state = stateV;
    }

    public static String getState() {
        return StringUtils.lowerCase(StringUtils.defaultIfBlank(state, "CA"));
    }

    public static void logout() {
        token = null;
        state = null;
        clientToken=null;
        isIndividual=false;
        clearEfile();
    }

    public static void clearEfile() {
        efile.clear();
        file.clear();
        case_party.clear();
        cross_reference.clear();
        service_contact.clear();
        caseCategoryCode.clear();
        documentType.clear();
        serviceContactEnable=false;
        filingCode.clear();
        filing.clear();
        clearCaseCategoryCode();
        clearFilingCode();
        envelopId=null;

    }

    public static void isAuthenticated() throws Exception {
        if(StringUtils.isBlank(token())){
            SH.shellHelper.printInfo("please login using this command -> authenticate -U abc@gmail.com -P pasWord -I 1142");
            throw new Exception("User not authenticated");
        }
    }

    public static boolean isInitialCase(){
        if(StringUtils.isNotBlank(getEfile(ParamName.CASE_NUMBER))){
            return false;
        }
        return true;
    }


    public static void initServiceContact(Map<String, Object> serviceContact) {
       service_contact.add(serviceContact);
    }
}
