package com.ezcollab.cli.http;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Response {


    private int status = 200;
    /**
     * Message
     */
    private String message;
    private Integer recordsTotal;
    private Integer recordsFiltered;

    Map<String, ?> item;

    List items;

    public Response() {
        super();
    }

    public Response(Map<String, ?> item) {
        this.item = item;
    }




    public void putItem(Map payload) {
        if (item == null) {
            item = new HashMap();
        }
        item.putAll(payload);
    }

    public void putItemMulti(Map payload) {
        if (items == null) {
            items = new ArrayList();
        }
        items.add(payload);
    }

    public void putItemMulti(List payload) {
        if (items == null) {
            items = new ArrayList();
        }
        items.addAll(payload);
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public Integer getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Integer recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Integer getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Integer recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public Map<String, ?> getItem() {
        return item;
    }

    public void setItem(Map<String, ?> item) {
        this.item = item;
    }

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
