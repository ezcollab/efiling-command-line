package com.ezcollab.cli.http;

import com.ezcollab.cli.config.LocalStorage;
import com.ezcollab.cli.config.SH;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RequestUtils {

    public Response restTemplateWithToken(String endpoint, HttpMethod httpMethod, Object request) throws Exception {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("authtoken", LocalStorage.token());
        HttpEntity httpEntity;
        if (request == null) {
            httpEntity = new HttpEntity(httpHeaders);
        } else {
            httpEntity = new HttpEntity(request, httpHeaders);
        }

        try {
            ResponseEntity<Response> responseEntity = restTemplate.exchange(endpoint, httpMethod, httpEntity, Response.class);
            resolveResponse(responseEntity.getBody());
            return responseEntity.getBody();
        } catch (Exception x) {
            throw x;
        }
    }

    public Response restTemplateWithClientToken(String endpoint, HttpMethod httpMethod, Object request) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("clienttoken", LocalStorage.clientToken());
        HttpEntity httpEntity;
        if (request == null) {
            httpEntity = new HttpEntity(httpHeaders);
        } else {
            httpEntity = new HttpEntity(request, httpHeaders);
        }

        try {
            ResponseEntity<Response> responseEntity = restTemplate.exchange(endpoint, httpMethod, httpEntity, Response.class);
            resolveResponse(responseEntity.getBody());
            return responseEntity.getBody();
        } catch (Exception x) {
            throw x;
        }
    }

    public void resolveResponse(Response response) throws Exception {
        if (response.getStatus() >= 300) {
            throw new Exception(response.getMessage());
        }
        if (StringUtils.isNotBlank(response.getMessage()) && response.getStatus() == 200) {
            SH.shellHelper.printSuccess(response.getMessage());
        }
    }


}
