package com.ezcollab.cli.http;

public class ParamName {




    public static final String JURISDICTION="jurisdiction";
    public static final String CASE_CATEGORY="case_category";
    public static final String CASE_TYPE="case_type";
    public static final String CASE_NUMBER="case_number";
    public static final String ATTORNEY="attorney";
    public static final String FILING_PARTY_NAME="filing_party_name";
    public static final String PAYMENT_ACCOUNT="payment_account";


    public static final String ECF_CASE_TYPE="ecf_case_type";
    public static final String FILER_TYPE="filer_type";

    //for case party
    public static final String CASE_PARTY="case_party";
    public static final String TYPE="type";
    public static final String BUSINESS_NAME="business_name";
    public static final String FIRST_NAME="first_name";
    public static final String PASSWORD="password";
    public static final String LAST_NAME="last_name";
    public static final String MIDDLE_NAME="middle_name";
    public static final String ADDRESS_CITY="address_city";
    public static final String ADDRESS_ONE="address_one";
    public static final String ADDRESS_STATE="address_state";
    public static final String ADDRESS_TWO="address_two";
    public static final String ADDRESS_ZIP="address_zip";
    public static final String PARTY_ID="party_id";
    public static final String IDENTIFICATION_ID="identification_id";

    public static final String FIRM_ADMIN_FIRST_NAME="firm_admin_first_name";
    public static final String FIRM_ADMIN_LAST_NAME="firm_admin_last_name";
    public static final String FIRM_ADMIN_MIDDLE_NAME="firm_admin_middle_name";
    public static final String FIRM_ADMIN_PASSWORD="firm_admin_password";

    //for filing
    public static final String FILING="filing";
    public static final String CODE="code";
    public static final String FILING_DESCRIPTION="filing_description";
    public static final String LEAD_DOC_CODE="lead_doc_code";
    public static final String LEAD_DOC_PATH="lead_doc_path";
    public static final String FILING_TYPE="filing_type";
    public static final String LEAD_DOC_SECURITY="lead_doc_security";

    //for damage amount
    public static final String DAMAGE_AMOUNT="damage_amount";
    public static final String PROCEDURE_REMEDY="procedure_remedy";
    public static final String IS_FILING_PARTY="is_filing_party";

    //for cross reference
    public static final String CROSS_REFERENCE="cross_reference";
    public static final String NUMBER="number";

    //service contact
    public static final String SERVICE_CONTACT="service_contact";

    public static final String BAR_NUMBER="bar_number";

    public static final String ADDRESS_LINE_1="address_line_1";
    public static final String ADDRESS_LINE_2="address_line_2";
    public static final String ADMINISTRATIVE_COPY="administrative_copy";
    public static final String CITY="city";
    public static final String COUNTRY="country";
    public static final String EMAIL="email";
    public static final String PHONE_NUMBER="phone_number";
    public static final String STATE="state";
    public static final String ZIP_CODE="zip_code";
    public static final String STATE_CODE="state_code";
    public static final String FIRM_NAME="firm_name";


    public static final String IS_FILER="is_filer";
    public static final String IS_FIRM_ADMIN="is_firm_admin";
}
