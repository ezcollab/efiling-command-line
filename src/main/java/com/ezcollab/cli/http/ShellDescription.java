package com.ezcollab.cli.http;

public class ShellDescription {

    public static final String EFILE_COMMAND = "Create a new case and file to court";
    //    public static final String RE_FILE_COMMAND="file a created case ";
    public static final String AUTHENTICATE_COMMAND = "Authenticate user using credential";
    public static final String AUTH_COMMAND = "Authenticate user by using properties set credential[not available in jar]";
    public static final String LOGOUT_COMMAND = "Logout currently logged user";
    public static final String STATE_COMMAND = "Display or modifies state in which operation will perform";


    public static final String EFILE_TEMPLATE_COMMAND = "create and file a new/existing case using template(json)";
    public static final String FILING_STATUS_COMMAND = "Displays a filing status of a case";
    public static final String CANCEL_COMMAND = "Cancel a filing";
    public static final String SEARCH_CASE_COMMAND = "Search for  existing case using case number and location";
    public static final String CASE_DETAIL_COMMAND = "Display case details of existing case";
    public static final String SHOW_FILING_COMMAND = "Display filings with 1 week duration";
    public static final String WHO_AM_I_COMMAND = "Display login user information";
    public static final String RESEND_ACTIVATION_COMMAND = "Resend activation email";
    public static final String RESET_PASSWORD_COMMAND = "Reset password";
    public static final String CREATE_FIRM_COMMAND = "Create a firm account";
    public static final String CREATE_PROSE_COMMAND = "Create a prose account";
    public static final String SHOW_PAYMENTS_COMMAND = "Display payment accounts";
    public static final String CREATE_ATTORNEY_COMMAND = "Create attorney user";
    public static final String DELETE_ATTORNEY_COMMAND = "Delete attorney user";
    public static final String SHOW_ATTORNEYS_COMMAND = "Display attorneys list";
    public static final String CREATE_SERVICE_CONTACT_COMMAND = "Create service contact";
    public static final String DELETE_SERVICE_CONTACT_COMMAND = "Delete service contact";
    public static final String SHOW_SERVICE_CONTACTS_COMMAND = "Display service contacts";
    public static final String SHOW_SERVICE_CONTACT_COMMAND = "Display service contact";

    public static final String SHOW_FIRM_INFO_COMMAND = "Display firm info";
    public static final String SHOW_FIRM_USERS_COMMAND = "Display firm users";
    public static final String CREATE_FIRM_USER_COMMAND = "Create new firm user";
    public static final String SHOW_FIRM_USER_COMMAND = "Display firm user";
    public static final String DELETE_FIRM_USER_COMMAND = "Delete/Remove firm user";


}
