package com.ezcollab.cli.command;


import com.ezcollab.cli.http.ShellDescription;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import static com.ezcollab.cli.config.SH.*;

@ShellComponent
public class Commands {


    @ShellMethod(value = ShellDescription.EFILE_COMMAND, group = "State Efiling Commands")
    public void efile() throws Exception {
        shellHelper.description(env.getProperty("efile.title"), env.getProperty("efile.description"));
        efileService.processManual();
    }

    @ShellMethod(value = ShellDescription.EFILE_TEMPLATE_COMMAND, group = "State Efiling Commands")
    public void efileTemplate(@ShellOption(value = {"-C", "--casenumber"}, defaultValue = "") String casenumber, @ShellOption({"-T", "--templatepath"}) String template, @ShellOption({"-L", "--locationcode"}) String locationcode, @ShellOption({"-D", "--filingdocument"}) String filingdocument) throws Exception {
        shellHelper.description(env.getProperty("efile.template.title"), env.getProperty("efile.template.description"));
        templateProcess.process(casenumber, locationcode, template, filingdocument);
    }

    @ShellMethod(value = ShellDescription.FILING_STATUS_COMMAND, group = "State Efiling Commands")
    public void filingStatus() throws Exception {
        shellHelper.description(env.getProperty("filing.status.title"), env.getProperty("filing.status.description"));
        efileService.filingStatus();
    }

    @ShellMethod(value = ShellDescription.CANCEL_COMMAND, group = "State Efiling Commands")
    public void cancelFiling() throws Exception {
        shellHelper.description(env.getProperty("cancel.filing.title"), env.getProperty("cancel.filing.description"));
        efileService.cancelFiling();
    }

    @ShellMethod(value = ShellDescription.SEARCH_CASE_COMMAND, group = "State Efiling Commands")
    public void searchCase() throws Exception {
        shellHelper.description(env.getProperty("case.search.title"), env.getProperty("case.search.description"));
        efileService.searchCase();
    }

    @ShellMethod(value = ShellDescription.CASE_DETAIL_COMMAND, group = "State Efiling Commands")
    public void caseDetails() throws Exception {
        shellHelper.description(env.getProperty("case.details.title"), env.getProperty("case.details.description"));
        efileService.caseDetails();
    }

    @ShellMethod(value = ShellDescription.SHOW_FILING_COMMAND, group = "State Efiling Commands")
    public void showFilings() throws Exception {
        shellHelper.description(env.getProperty("show.filings.title"), env.getProperty("show.filings.description"));
        efileService.filingList();
    }


    @ShellMethod(value = ShellDescription.AUTHENTICATE_COMMAND, group = "Account Commands")
    private void authenticate(@ShellOption({"-U", "--username"}) String username, @ShellOption({"-P", "--password"}) String password, @ShellOption({"-I", "--clienttoken"}) String clienttoken) throws Exception {
        shellHelper.description(env.getProperty("authenticate.title"), env.getProperty("authenticate.description"));
        efsp.getAuthToken(username, password, clienttoken);
    }

   /* @ShellMethod(value = ShellDescription.AUTH_COMMAND, group = "Account Commands")
    private void auth() throws Exception {
        shellHelper.description(env.getProperty("auth.title"), env.getProperty("auth.description"));
        accountService.loginUsingProperties();
    }*/

    @ShellMethod(value = ShellDescription.LOGOUT_COMMAND, group = "Account Commands")
    public void logout() throws Exception {
        shellHelper.description(env.getProperty("logout.title"), env.getProperty("logout.description"));
        accountService.logout();
    }

    @ShellMethod(value = ShellDescription.WHO_AM_I_COMMAND, group = "Account Commands")
    public void whoami() throws Exception {
        shellHelper.description(env.getProperty("whoami.title"), env.getProperty("whoami.description"));
        accountService.whoami();
    }

    @ShellMethod(value = ShellDescription.RESEND_ACTIVATION_COMMAND, group = "Account Commands")
    private void resendActivationEmail() throws Exception {
        shellHelper.description(env.getProperty("resend.activation.email.title"), env.getProperty("resend.activation.email.description"));
        accountService.selfResendActivationEmail();
    }

    @ShellMethod(value = ShellDescription.RESET_PASSWORD_COMMAND, group = "Account Commands")
    private void resetPassword() throws Exception {
        shellHelper.description(env.getProperty("reset.password.title"), env.getProperty("reset.password.description"));
        accountService.resetPasswordRequest();
    }

    @ShellMethod(value = ShellDescription.CREATE_FIRM_COMMAND, group = "Account Commands")
    private void createFirmAccount() throws Exception {
        shellHelper.description(env.getProperty("create.firm.account.title"), env.getProperty("create.firm.account.description"));
        accountService.createNewFirmAdministratorAccount();
    }

    @ShellMethod(value = ShellDescription.CREATE_PROSE_COMMAND, group = "Account Commands")
    private void createProseAccount() throws Exception {
        shellHelper.description(env.getProperty("create.prose.account.title"), env.getProperty("create.prose.account.description"));
        accountService.createNewIndividualAccount();
    }

    @ShellMethod(value = ShellDescription.STATE_COMMAND, group = "Account Commands")
    private void state(@ShellOption(value = {"-S", "--state"}, defaultValue = "") String stateCode) throws Exception {
        shellHelper.description(env.getProperty("state.title"), env.getProperty("state.description"));
        accountService.state(stateCode);
    }


    @ShellMethod(value = ShellDescription.SHOW_PAYMENTS_COMMAND, group = "Payment Account")
    private void showPayment() throws Exception {
        shellHelper.description(env.getProperty("show.payments.title"), env.getProperty("show.payments.description"));
        paymentService.list();
    }

    @ShellMethod(value = ShellDescription.CREATE_ATTORNEY_COMMAND, group = "Attorney Account")
    private void createAttorney() throws Exception {
        shellHelper.description(env.getProperty("create.attorney.title"), env.getProperty("create.attorney.description"));
        attorneyService.create();
    }

    @ShellMethod(value = ShellDescription.DELETE_ATTORNEY_COMMAND, group = "Attorney Account")
    private void deleteAttorney() throws Exception {
        shellHelper.description(env.getProperty("delete.attorney.title"), env.getProperty("delete.attorney.description"));
        attorneyService.delete();
    }

    @ShellMethod(value = ShellDescription.SHOW_ATTORNEYS_COMMAND, group = "Attorney Account")
    private void showAttorney() throws Exception {
        shellHelper.description(env.getProperty("show.attorneys.title"), env.getProperty("show.attorneys.description"));
        attorneyService.list();
    }


    @ShellMethod(value = ShellDescription.CREATE_SERVICE_CONTACT_COMMAND, group = "Service Contact Commands")
    private void createServiceContact() throws Exception {
        shellHelper.description(env.getProperty("create.service.contact.title"), env.getProperty("create.service.contact.description"));
        serviceContactService.create();
    }

    @ShellMethod(value = ShellDescription.DELETE_SERVICE_CONTACT_COMMAND, group = "Service Contact Commands")
    private void deleteServiceContact() throws Exception {
        shellHelper.description(env.getProperty("delete.service.contact.title"), env.getProperty("delete.service.contact.description"));
        serviceContactService.delete();
    }

    @ShellMethod(value = ShellDescription.SHOW_SERVICE_CONTACTS_COMMAND, group = "Service Contact Commands")
    private void showServiceContact() throws Exception {
        shellHelper.description(env.getProperty("show.service.contacts.title"), env.getProperty("show.service.contacts.description"));
        serviceContactService.list();
    }

    @ShellMethod(value = ShellDescription.SHOW_SERVICE_CONTACT_COMMAND, group = "Service Contact Commands")
    private void serviceContact() throws Exception {
        shellHelper.description(env.getProperty("show.service.contact.title"), env.getProperty("show.service.contact.description"));
        serviceContactService.getServiceContactUser();
    }

    @ShellMethod(value = ShellDescription.SHOW_FIRM_INFO_COMMAND, group = "Firm Commands")
    private void firmInfo() throws Exception {
        shellHelper.description(env.getProperty("show.firm.info.title"), env.getProperty("show.firm.info.description"));
        firmUserService.firmInfo();
    }

    @ShellMethod(value = ShellDescription.SHOW_FIRM_USERS_COMMAND, group = "Firm Commands")
    private void firmUserList() throws Exception {
        shellHelper.description(env.getProperty("show.firm.users.title"), env.getProperty("show.firm.users.description"));
        firmUserService.firmUserList();
    }

    @ShellMethod(value = ShellDescription.CREATE_FIRM_USER_COMMAND, group = "Firm Commands")
    private void createFirmUser() throws Exception {
        shellHelper.description(env.getProperty("create.firm.user.title"), env.getProperty("create.firm.user.description"));
        firmUserService.createFirmUser();
    }

    @ShellMethod(value = ShellDescription.SHOW_FIRM_USER_COMMAND, group = "Firm Commands")
    private void firmUserInfo() throws Exception {
        shellHelper.description(env.getProperty("show.firm.user.title"), env.getProperty("show.firm.user.description"));
        firmUserService.getFirmUser();
    }

    @ShellMethod(value = ShellDescription.DELETE_FIRM_USER_COMMAND, group = "Firm Commands")
    private void deleteFirm() throws Exception {
        shellHelper.description(env.getProperty("delete.firm.user.title"), env.getProperty("delete.firm.user.description"));
        firmUserService.deleteFirmUser();
    }


}
