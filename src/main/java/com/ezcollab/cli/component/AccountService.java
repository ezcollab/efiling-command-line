package com.ezcollab.cli.component;

import com.ezcollab.cli.config.LocalStorage;
import com.ezcollab.cli.http.ParamName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.*;
import static com.ezcollab.cli.config.SH.*;

@SuppressWarnings("ALL")
@Component
public class AccountService {

    public void loginUsingProperties() throws Exception {
        String username= env.getProperty("shell.username");
        String password=env.getProperty("shell.password");
        String clienttoken=env.getProperty("shell.clienttoken");
        if(StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password) && StringUtils.isNotBlank(clienttoken)){
            efsp.getAuthToken(username, password, clienttoken);
            return;
        }
        shellHelper.printError("You have to initialize credentials in properties so please use authenticate command.");
    }

    public void logout(){
        if(StringUtils.isNotBlank(token())){
            LocalStorage.logout();
            shellHelper.printSuccess("You have successfully logout.");
            return;
        }
        shellHelper.printError("You have not logged in yet, use authenticate command to login.");
    }
    public void state(String state) throws Exception {
        Map<String, String> stateMap = new HashMap<>();
        stateMap.put("TX", "TX");
        stateMap.put("CA", "CA");
        stateMap.put("IL", "IL");

        shellHelper.printSuccess("Current state " + getState() + ".");
        if (StringUtils.isBlank(state)) {

            boolean stateChange = shellUtils.promptWithOptions("Do you want to change the state?");
            if (stateChange) {
                initState(shellUtils.selectFromList(stateMap, "Select State for processing", "Change state", true));
                shellHelper.printSuccess("You have successfully set " + getState());
            }
            return;
        }
        if (stateMap.containsKey(state)) {
            initState(state);
            shellHelper.printSuccess("You have successfully set " + getState());
        }
    }
    public void whoami() throws Exception {
        isAuthenticated();
        Object[][] data=tableViewResolver.convertToShellTable(efsp.whoami());
        shellHelper.table(data);
    }

    public void selfResendActivationEmail() throws Exception {
        String email=shellUtils.singleLine("Enter your email address",null,true);
        Map map=new HashMap();
        map.put(ParamName.EMAIL,email);
        efsp.selfResendActivationEmail(map);
    }

    public void resetPasswordRequest() throws Exception {
        String email=shellUtils.singleLine("Enter your email address",null,true);
        Map map=new HashMap();
        map.put(ParamName.EMAIL,email);
        efsp.resetPasswordRequest(map);
    }

    public void createNewFirmAdministratorAccount() throws Exception {
        Map data = new HashMap();


        data.put(ParamName.EMAIL, shellUtils.singleLine("Enter your email address", null, true));

        String password = shellUtils.singleLine("Enter your password", null, true);
        data.put(ParamName.PASSWORD, password);

        data.put(ParamName.FIRM_ADMIN_FIRST_NAME, shellUtils.singleLine("Enter your first name", null, true));
        data.put(ParamName.FIRM_ADMIN_LAST_NAME, shellUtils.singleLine("Enter your last name", null, true));
        data.put(ParamName.FIRM_ADMIN_MIDDLE_NAME, shellUtils.singleLine("Enter your middle name", null, false));
        data.put(ParamName.FIRM_ADMIN_PASSWORD, password);


        data.put(ParamName.FIRM_NAME,  shellUtils.singleLine("Enter your firm name", null, true));

        boolean isAddress = shellUtils.promptWithOptions("Is address available?");
        String city = "";
        String address1 = "";
        String state = "";
        String address2 = "";
        String zip = "";
        if (isAddress) {
            city = shellUtils.singleLine("Entery city name", null, true);
            address1 = shellUtils.singleLine("Enter address one ", null, true);
            state = shellUtils.singleLine("Enter state", null, true);
            address2 = shellUtils.singleLine("Enter address Two", null, true);
            zip = shellUtils.singleLine("Enter zip code", null, true);
        }
        data.put(ParamName.CITY, city);
        data.put(ParamName.ADDRESS_LINE_1, address1);
        data.put(ParamName.ADDRESS_LINE_2, state);
        data.put(ParamName.STATE_CODE, address2);
        data.put(ParamName.ZIP_CODE, zip);
        efsp.createNewFirmAdministratorAccount(data);
    }

    public void createNewIndividualAccount() throws Exception {
        Map data = new HashMap();


        data.put(ParamName.EMAIL, shellUtils.singleLine("Enter your email address", null, true));

        data.put(ParamName.PASSWORD, shellUtils.singleLine("Enter your password", null, true));

        data.put(ParamName.FIRST_NAME, shellUtils.singleLine("Enter your first name", null, true));
        data.put(ParamName.LAST_NAME,  shellUtils.singleLine("Enter your last name", null, true));
        data.put(ParamName.MIDDLE_NAME, shellUtils.singleLine("Enter your middle name", null, false));


        boolean isAddress = shellUtils.promptWithOptions("is address available?");
        String city = "";
        String address1 = "";
        String state = "";
        String address2 = "";
        String zip = "";
        if (isAddress) {
            city = shellUtils.singleLine("City", null, true);
            address1 = shellUtils.singleLine("Address one ", null, true);
            state = shellUtils.singleLine("State", null, true);
            address2 = shellUtils.singleLine("Address Two", null, true);
            zip = shellUtils.singleLine("Zip", null, true);
        }
        data.put(ParamName.CITY, city);
        data.put(ParamName.ADDRESS_LINE_1, address1);
        data.put(ParamName.ADDRESS_LINE_2, state);
        data.put(ParamName.STATE_CODE, address2);
        data.put(ParamName.ZIP_CODE, zip);
        efsp.createNewIndividualAccount(data);
    }
}
