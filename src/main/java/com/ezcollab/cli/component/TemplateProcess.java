package com.ezcollab.cli.component;

import com.ezcollab.cli.http.ParamName;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.*;
import static com.ezcollab.cli.config.SH.*;

@Component
public class TemplateProcess {

    public void process(String casenumber, String locationcode, String templatepath, String filingdocPath) throws Exception {
        clearEfile();
        if (StringUtils.isNotBlank(casenumber)) {
            shellHelper.printInfo("Processing Existing Case");
            String caseTrackingId = efileService.searchCase(locationcode, casenumber);
            Map<String, ?> caseDetails = efileService.caseDetail(caseTrackingId);
            initEfile(ParamName.CASE_NUMBER, casenumber);
            initEfile(ParamName.CASE_NUMBER, casenumber);
            initEfile(ParamName.JURISDICTION, locationcode);
            //processExistingCaseParties(caseDetails, providingFilingPartyId(templatepath));

            processExisingCaseInfo(caseDetails);
            processExistingTemplateData(templatepath);
        } else {
            shellHelper.printInfo("Processing New Case");
            JSONObject object = processTemplatePath(templatepath);
            processFiling(object);
            processCaseParties(object);
            processServiceContact(object);
            processCrossReference(object);
            initEfile(ParamName.JURISDICTION, locationcode);
            processInfo(object);
        }
        processExisingFilings(filingdocPath);
        efileService.summary();
        efileService.submit();
    }

    private void processExisingFilings(String filingdocPath) throws IOException {
        String data = new String(Files.readAllBytes(Paths.get(filingdocPath)), StandardCharsets.UTF_8);
        JSONArray jsonArray = new JSONArray(data);
        for (Object jsonObject : jsonArray) {
            JSONObject jsonObj = (JSONObject) jsonObject;
            initFile(jsonObj.getString("lead_doc_path"));
        }
    }

    private void processExisingCaseInfo(Map<String, ?> caseDetails) {
        initEfile(ParamName.CASE_CATEGORY, (String) caseDetails.get("caseCategory"));
        initEfile(ParamName.CASE_TYPE, (String) caseDetails.get("caseType"));
        initEfile(ParamName.FILER_TYPE, (String) caseDetails.get("filerType"));

    }

    private void processExistingTemplateData(String templatePath) throws Exception {
        //get from template or data
        JSONObject templateObject = processTemplatePath(templatePath);
        code.caseCategory();  //used for efc case type fetch purpose
        initEfile(ParamName.FILING_PARTY_NAME, templateObject.getString(ParamName.FILING_PARTY_NAME));
        initEfile(ParamName.PAYMENT_ACCOUNT, templateObject.getString(ParamName.PAYMENT_ACCOUNT));
        initEfile(ParamName.ATTORNEY, templateObject.getString(ParamName.ATTORNEY));
        processFiling(templateObject);
        processCaseParties(templateObject);
        processServiceContact(templateObject);
    }


    /*private void processExistingCaseParties(Map<String, ?> caseDetails, String filingPartyId) {
        JSONObject casePartiesObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        List<Map<String, ?>> caseParties = (List<Map<String, ?>>) caseDetails.get("caseParties");
        boolean isFilingPartyFound = false;
        for (Map<String, ?> caseParty : caseParties) {
            JSONObject finalParty = new JSONObject();
            finalParty.put(ParamName.TYPE, caseParty.get("partyType"));
            finalParty.put(ParamName.BUSINESS_NAME, caseParty.get("businessName"));
            finalParty.put(ParamName.FIRST_NAME, caseParty.get("firstName"));
            finalParty.put(ParamName.LAST_NAME, caseParty.get("lastName"));
            finalParty.put(ParamName.MIDDLE_NAME, caseParty.get("middleName"));
            if (StringUtils.isNotBlank((CharSequence) caseParty.get("address"))) {
                JSONObject addressObject = new JSONObject(caseParty.get("address"));
                finalParty.put(ParamName.ADDRESS_CITY, addressObject.getString("city"));
                finalParty.put(ParamName.ADDRESS_ONE, addressObject.getString("addressLine1"));
                finalParty.put(ParamName.ADDRESS_STATE, addressObject.getString("state"));
                finalParty.put(ParamName.ADDRESS_TWO, addressObject.getString("addressLine2"));
                finalParty.put(ParamName.ADDRESS_ZIP, addressObject.getString("zipCode"));
            }
            finalParty.put(ParamName.IDENTIFICATION_ID, caseParty.get("identificationID"));


            //check particular case party is filing party or not filing_party_id
            boolean isFilingPartyProvided = StringUtils.isNotBlank(filingPartyId) && StringUtils.equalsIgnoreCase((CharSequence) caseParty.get("identificationID"), filingPartyId);
            if ((((Boolean) caseParty.get("filingParty")) && !isFilingPartyFound) || (isFilingPartyProvided && !isFilingPartyFound)) {
                isFilingPartyFound = true;
                finalParty.put(ParamName.IS_FILING_PARTY, "true");
            }

            finalParty.put(ParamName.PARTY_ID, "");
            jsonArray.put(finalParty);

        }
        casePartiesObject.put("case_party", jsonArray);
        processCaseParties(casePartiesObject);
    }*/

   /* private String providingFilingPartyId(String templatePath) throws IOException {
        JSONObject object = processTemplatePath(templatePath);
        return object.getString("filing_party_id");
    }*/

    private JSONObject processTemplatePath(String templatePath) throws IOException {
        String data = new String(Files.readAllBytes(Paths.get(templatePath)), StandardCharsets.UTF_8);
        return new JSONObject(data);
    }

    private void processFiling(JSONObject object) {
        JSONArray filingJsonArray = object.getJSONArray("filing");
        for (Object obj : filingJsonArray) {
            JSONObject jo = (JSONObject) obj;
            Map<String, Object> finalMap = new HashMap<>();
            createMap(jo, finalMap);
            initFiling(finalMap);
        }
    }

    private void processCaseParties(JSONObject object) {
        //case party
        JSONArray casePartyJsonArray = object.getJSONArray("case_party");
        for (Object obj : casePartyJsonArray) {
            JSONObject jo = (JSONObject) obj;
            Map<String, Object> finalMap = new HashMap<>();
            createMap(jo, finalMap);
            initCaseParty(finalMap);
        }
    }

    private void processCrossReference(JSONObject object) {
        //cross reference
        JSONArray crossReferenceJsonArray = object.getJSONArray("cross_reference");
        for (Object obj : crossReferenceJsonArray) {
            JSONObject jo = (JSONObject) obj;
            Map<String, Object> finalMap = new HashMap<>();
            createMap(jo, finalMap);
            initCrossRef(finalMap);
        }
    }

    private void processServiceContact(JSONObject object) {
        //cross reference
        JSONArray serviceContactJsonArray = object.getJSONArray("service_contact");
        for (Object obj : serviceContactJsonArray) {
            JSONObject jo = (JSONObject) obj;
            Map<String, Object> finalMap = new HashMap<>();
            createMap(jo, finalMap);
            initServiceContact(finalMap);
        }
    }

    private void processInfo(JSONObject object) {
        initEfile(ParamName.CASE_CATEGORY, object.getString(ParamName.CASE_CATEGORY));
        initEfile(ParamName.CASE_TYPE, object.getString(ParamName.CASE_TYPE));
        initEfile(ParamName.ECF_CASE_TYPE, object.getString(ParamName.ECF_CASE_TYPE));
        initEfile(ParamName.FILING_PARTY_NAME, object.getString(ParamName.FILING_PARTY_NAME));
        initEfile(ParamName.FILER_TYPE, object.getString(ParamName.FILER_TYPE));
        initEfile(ParamName.PAYMENT_ACCOUNT, object.getString(ParamName.PAYMENT_ACCOUNT));
        initEfile(ParamName.ATTORNEY, object.getString(ParamName.ATTORNEY));
    }


    private void createMap(JSONObject jo, Map data) {
        JSONArray names = jo.names();
        for (int k = 0; k < names.length(); k++) {
            String key = names.getString(k);
            Object valueObj = jo.get(key);
            data.put(key, valueObj);
        }
    }


}
