package com.ezcollab.cli.component;

import com.ezcollab.cli.http.ParamName;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.isAuthenticated;
import static com.ezcollab.cli.config.SH.*;

@SuppressWarnings("ALL")
@Component
public class ServiceContactService {


    public String select() throws Exception {
        isAuthenticated();
        List<Map<String, ?>> serviceContactList = efsp.serviceContactList();
        Map<String, String> serviceContactMap = new HashMap<>();
        for (Map<String, ?> map : serviceContactList) {
            serviceContactMap.put((String) map.get("service_contact_id"), map.get("first_name") + " " + map.get("middle_name") + " " + map.get("last_name"));
        }
        String serviceId = shellUtils.selectFromList(serviceContactMap, "Choose service contact from list", "Select service contact ", true);
        return serviceId;
    }


    public void list() throws Exception {
        isAuthenticated();
        List<Map<String, ?>> serviceContactList = efsp.serviceContactList();
        Object[][] dataObject = tableViewResolver.convertToShellTable(serviceContactList);
        shellHelper.printInfo("Service contact list");
        shellHelper.table(dataObject);
    }

    public void create() throws Exception {
        isAuthenticated();
        Map data = new HashMap();

        String firstName = shellUtils.singleLine("Enter first name", null, true);
        String lastName = shellUtils.singleLine("Enter last name", null, true);
        String middleName = shellUtils.singleLine("Enter middle ame", null, false);
        String email = shellUtils.singleLine("Enter email address", null, true);
        data.put(ParamName.EMAIL, email);


        data.put(ParamName.FIRST_NAME, firstName);
        data.put(ParamName.LAST_NAME, lastName);
        data.put(ParamName.MIDDLE_NAME, middleName);

        boolean isAddress = shellUtils.promptWithOptions("Is address available?");
        String city = "";
        String address1 = "";
        String state = "";
        String address2 = "";
        String zip = "";
        if (isAddress) {
            city = shellUtils.singleLine("Enter city", null, true);
            address1 = shellUtils.singleLine("Enter address one ", null, true);
            state = shellUtils.singleLine("Enter state", null, true);
            address2 = shellUtils.singleLine("Enter address two", null, true);
            zip = shellUtils.singleLine("Enter zip", null, true);
        }
        data.put(ParamName.CITY, city);
        data.put(ParamName.ADDRESS_LINE_1, address1);
        data.put(ParamName.ADDRESS_LINE_2, state);
        data.put(ParamName.STATE, address2);
        data.put(ParamName.ZIP_CODE, zip);


        String administrativeCopy = shellUtils.singleLine("Enter administrative copy email", null, true);
        data.put(ParamName.ADMINISTRATIVE_COPY, administrativeCopy);

        String phoneNumber = shellUtils.singleLine("Enter phone number", null, true);
        data.put(ParamName.PHONE_NUMBER, phoneNumber);

        data.put(ParamName.COUNTRY, "US");


        efsp.createServiceContactUser(data);
        list();
    }

    public void delete() throws Exception {
        isAuthenticated();
        String id = select();
        efsp.deleteServiceContactUser(id);

    }

    public void getServiceContactUser() throws Exception {
        isAuthenticated();
        String id = shellUtils.singleLine("Enter service contact id", null, true);

        Object[][] data=tableViewResolver.convertToShellTable(efsp.getServiceContactUser(id));
        shellHelper.table(data);
    }
}
