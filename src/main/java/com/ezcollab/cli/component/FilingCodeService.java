package com.ezcollab.cli.component;


import com.ezcollab.cli.http.ParamName;
import com.ezcollab.cli.http.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.*;
import static com.ezcollab.cli.config.SH.requestUtils;
import static com.ezcollab.cli.config.SH.shellHelper;
import static com.ezcollab.cli.utils.EndpointUtils.getEndpoint;

@SuppressWarnings("Duplicates")
@Service
public class FilingCodeService {


    public Map<String, String> locationCode() throws Exception {
        isAuthenticated();
        shellHelper.loading("location codes");
        String url = getEndpoint("uslegalpro.code.locationcode.list");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            finalMap.put(map.get("code"), map.get("name"));
        }
        return finalMap;
    }

    public Map<String, String> caseCategory() throws Exception {
        isAuthenticated();
        shellHelper.loading("case category codes");
        clearCaseCategoryCode();
        String url = getEndpoint("uslegalpro.code.casecategory.list") + "?location_code=" + getEfile(ParamName.JURISDICTION);
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            finalMap.put(map.get("code"), map.get("name"));
            initCaseCategoryCode(map.get("code"), map);
        }
        return finalMap;


    }

    public Map<String, String> caseType() throws Exception {
        isAuthenticated();
        shellHelper.loading("case type codes");
        String isInitial = "true";
        if (StringUtils.isNotBlank(getEfile(ParamName.CASE_NUMBER))) {
            isInitial = "false";
        }
        String url = getEndpoint("uslegalpro.code.casetype.list") + "?location_code=" + getEfile(ParamName.JURISDICTION) + "&case_category_code=" + getEfile(ParamName.CASE_CATEGORY) + "&is_initial=" + isInitial;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            finalMap.put(map.get("code"), map.get("name"));
        }
        return finalMap;


    }

    public Map<String, String> partyTypeCode(boolean isRequiredOnly) throws Exception {
        isAuthenticated();
        shellHelper.loading("party type codes");
        String url = getEndpoint("uslegalpro.code.partytype.list") + "?location_code=" + getEfile(ParamName.JURISDICTION) + "&case_type_id=" + getEfile(ParamName.CASE_TYPE);
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            if (isRequiredOnly) {
                if (StringUtils.equalsIgnoreCase(map.get("is_required"), "true")) {
                    finalMap.put(map.get("code"), map.get("name"));
                }
                continue;
            }
            finalMap.put(map.get("code"), map.get("name"));
        }
        return finalMap;


    }


    public Map<String, String> filingCode() throws Exception {
        isAuthenticated();
        shellHelper.loading("filing codes");
        clearFilingCode();
        String filingType = "Initial";
        if (StringUtils.isNotBlank(getEfile(ParamName.CASE_NUMBER))) {
            filingType = "Subsequent";
        }

        String url = getEndpoint("uslegalpro.code.filingcode.list") + "?location_code=" + getEfile(ParamName.JURISDICTION) + "&case_category=" + getEfile(ParamName.CASE_CATEGORY) + "&case_type=" + getEfile(ParamName.CASE_TYPE) + "&filing_type=" + filingType;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            finalMap.put(map.get("code"), map.get("name"));
            initFilingCode(map.get("code"), map);
        }
        return finalMap;


    }

    public Map<String, String> filingDocuemnt(String filingCode) throws Exception {
        isAuthenticated();
        shellHelper.loading("filing documents");
        String url = getEndpoint("uslegalpro.code.filingcomponent.list") + "?location_code=" + getEfile(ParamName.JURISDICTION) + "&filingCode=" + filingCode;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            finalMap.put(map.get("code"), map.get("name"));
        }
        return finalMap;


    }

    public Map<String, String> documentType(String filingCode) throws Exception {
        isAuthenticated();
        shellHelper.loading("document types");
        clearDocumentType();
        String url = getEndpoint("uslegalpro.code.documenttype.list") + "?location_code=" + getEfile(ParamName.JURISDICTION) + "&filing_code=" + filingCode;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            initDocumentType(map.get("code"), map.get("name"));
        }
        return finalMap;
    }

    public Map<String, String> crossReferenceCode(boolean isRequiredOnly) throws Exception {
        isAuthenticated();
        shellHelper.loading("cross reference codes");
        clearDocumentType();
        String url = getEndpoint("uslegalpro.code.crossreferencecode.list") + "?location_code=" + getEfile(ParamName.JURISDICTION) + "&case_type_code=" + getEfile(ParamName.CASE_CATEGORY);
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        Map<String, String> finalMap = new HashMap<>();
        for (Map<String, String> map : (List<Map<String, String>>) response.getItems()) {
            if (isRequiredOnly) {
                if (StringUtils.equalsIgnoreCase(map.get("is_required"), "true")) {
                    finalMap.put(map.get("code"), map.get("name"));
                }
                continue;
            }
            finalMap.put(map.get("code"), map.get("name"));
        }
        return finalMap;
    }
    public Map<String, String> getFilingType(boolean isInitial) throws Exception {
        isAuthenticated();
        clearDocumentType();
        String url = getEndpoint("uslegalpro.code.filingtype") + "?is_initial=" + isInitial;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return (Map<String, String>) response.getItem();

    }
}
