package com.ezcollab.cli.component;


import com.ezcollab.cli.config.LocalStorage;
import com.ezcollab.cli.config.SH;
import com.ezcollab.cli.http.Request;
import com.ezcollab.cli.http.Response;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.*;
import static com.ezcollab.cli.config.SH.*;
import static com.ezcollab.cli.utils.EndpointUtils.getEndpoint;


@SuppressWarnings("ALL")
@Service
public class EFSPService {


    public void getAuthToken(String username, String password, String clienttoken) throws Exception {
        shellHelper.printInfo("Authenticating user");
        LocalStorage.logout();
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> dataMap = new HashMap<>();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add("clienttoken", clienttoken);

        dataMap.put("username", username);
        dataMap.put("password", password);

        /*
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<Response> responseEntity = restTemplate.exchange(getEndpoint("uslegalpro.authenticate") + "?username=" + username + "&password=" + password, HttpMethod.POST, httpEntity, Response.class);
        Response response=responseEntity.getBody();
        if(response.getStatus()>=400){
            throw new Exception(response.getMessage());
        }
        initClientToken(clienttoken);
        String token = (String) response.getItem().get("auth_token");
        */
        String token = "13ee73bc-77c4-46de-8f92-51f8a79a7810/ULPSTAG/d59cdd05-4d82-44b5-8d97-2495f227e560";
        initToken(token);

        Map firmInfo=firmInfo();
        setIndividual((Boolean) Boolean.valueOf((String)firmInfo.get("is_individual")));
        shellHelper.printSuccess("You have successfully logged in");
    }

    public Response submitFiling(String envelopId) throws Exception {
        shellHelper.printInfo("Submitting filing");
        Response response = requestUtils.restTemplateWithToken(getEndpoint("uslegalpro.efile.case-envelopId") + envelopId, HttpMethod.GET, null);
        return response;
    }

    public Response submitFiling() throws Exception {
        shellHelper.printInfo("Submitting filing");
        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        for (String filePath : getFile()) {
            body.add("file", fileConvert(filePath));
        }
        Map metaData = new HashMap<>();
        metaData.put("data", completeEfile());
        JSONObject jsonObject = new JSONObject(metaData);
        body.add("meta_data", jsonObject.toString());

        Response response = requestUtils.restTemplateWithToken(getEndpoint("uslegalpro.efile.case"), HttpMethod.POST, body);
        return response;
    }

    private FileSystemResource fileConvert(String completePath) throws IOException {
        FileSystemResource resource = new FileSystemResource(new File(completePath));
        return resource;
    }

    public List<Map<String, ?>> paymentAccounts() throws Exception {
        shellHelper.loading(" payment accounts");
        Response response = requestUtils.restTemplateWithToken(getEndpoint("uslegalpro.paymentaccount.list"), HttpMethod.GET, null);
        return response.getItems();
    }

    public List attorneyAccounts() throws Exception {
        shellHelper.loading(" attorney accounts");
        Response response = requestUtils.restTemplateWithToken(getEndpoint("uslegalpro.attorneys.list"), HttpMethod.GET, null);
        return response.getItems();
    }

    public Response feeCalculation() throws Exception {
        shellHelper.printInfo(" calculating fee");
        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        for (String filePath : getFile()) {
            body.add("file", fileConvert(filePath));
        }
        Map metaData = new HashMap<>();
        metaData.put("data", completeEfile());
        body.add("meta_data", metaData);
        Response response = requestUtils.restTemplateWithToken(getEndpoint("uslegalpro.efile.feecalculation"), HttpMethod.POST, body);
        return response;
    }

    public Map<String, ?> filingStatus(String filingId) throws Exception {
        String url = getEndpoint("uslegalpro.efile.status") + filingId;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItem();
    }

    public Map<String, ?> cancelFiling(String filingId) throws Exception {
        String url = getEndpoint("uslegalpro.efile.cancel") + filingId;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.DELETE, null);
        return response.getItem();
    }

    public String searchCase(String locationcode, String caseNumber) throws Exception {
        String url = getEndpoint("uslegalpro.efile.search-case") + "?location_code=" + locationcode + "&case_number=" + caseNumber;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getMessage();
    }

    public Map<String, ?> caseDetails(String caseTrackingId) throws Exception {
        String url = getEndpoint("uslegalpro.efile.case-detail") + caseTrackingId;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItem();
    }

    public List<Map<String, ?>> filingList() throws Exception {
        String url = getEndpoint("uslegalpro.efile.filing-list");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItems();
    }

    public Map<String, ?> firmInfo() throws Exception {
        String url = getEndpoint("uslegalpro.firm.info"); // TODO: 6/16/2019
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItem();
    }

    public List<Map<String, ?>> firmUserList() throws Exception {
        String url = getEndpoint("uslegalpro.firm.user.list");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItems();
    }

    public Map<String, ?> whoami() throws Exception {
        String url = getEndpoint("uslegalpro.account.whoami");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItem();
    }

    public void selfResendActivationEmail(Map data) throws Exception {
        String url = getEndpoint("uslegalpro.account.resend-activation-email");
        Response response = requestUtils.restTemplateWithClientToken(url, HttpMethod.POST, new Request(data));

    }

    public void resetPasswordRequest(Map data) throws Exception {
        String url = getEndpoint("uslegalpro.account.reset-password");
        Response response = requestUtils.restTemplateWithClientToken(url, HttpMethod.POST, new Request(data));

    }

    public void createNewFirmAdministratorAccount(Map data) throws Exception {
        String url = getEndpoint("uslegalpro.account.firm");
        Response response = requestUtils.restTemplateWithClientToken(url, HttpMethod.POST, new Request(data));

    }

    public void createNewIndividualAccount(Map data) throws Exception {
        String url = getEndpoint("uslegalpro.account.prose");
        Response response = requestUtils.restTemplateWithClientToken(url, HttpMethod.POST, new Request(data));

    }

    public void createFirmUser(Map data) throws Exception {
        String url = getEndpoint("uslegalpro.firm.user.create");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.POST, new Request(data));
    }

    public Map<String, ?> getFirmUser() throws Exception {
        String url = getEndpoint("uslegalpro.firm.info");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItem();
    }

    public void deleteFirmUser(String id) throws Exception {
        String url = getEndpoint("uslegalpro.firm.user.delete") + id;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.DELETE, null);
    }

    public void createAttorneyUser(Map data) throws Exception {
        String url = getEndpoint("uslegalpro.attorneys.create");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.POST, new Request(data));
    }

    public void deleteAttorneyUser(String attorneyId) throws Exception {
        String url = getEndpoint("uslegalpro.attorneys.delete") + attorneyId;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.DELETE, null);
    }

    public void createServiceContactUser(Map data) throws Exception {
        String url = getEndpoint("uslegalpro.servicecontact.create");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.POST, new Request(data));
    }

    public Map<String,?> getServiceContactUser(String id) throws Exception {
        String url = getEndpoint("uslegalpro.servicecontact.get") + id;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItem();
    }

    public List<Map<String, ?>> serviceContactList() throws Exception {
        String url = getEndpoint("uslegalpro.servicecontact.list");
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.GET, null);
        return response.getItems();
    }

    public void deleteServiceContactUser(String id) throws Exception {
        String url = getEndpoint("uslegalpro.servicecontact.delete") + id;
        Response response = requestUtils.restTemplateWithToken(url, HttpMethod.DELETE, null);
    }

}
