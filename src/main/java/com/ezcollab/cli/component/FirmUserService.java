package com.ezcollab.cli.component;

import com.ezcollab.cli.http.ParamName;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.isAuthenticated;
import static com.ezcollab.cli.config.SH.*;

@SuppressWarnings("ALL")
@Component
public class FirmUserService {


    public void firmInfo() throws Exception {
        isAuthenticated();
        Object[][] data = tableViewResolver.convertToShellTable(efsp.firmInfo());
        shellHelper.table(data);
    }

    public void firmUserList() throws Exception {
        isAuthenticated();
        Object[][] data = tableViewResolver.convertToShellTable(efsp.firmUserList());
        shellHelper.table(data);
    }

    public void createFirmUser() throws Exception {
        isAuthenticated();

        Map data = new HashMap();

        String firstName = shellUtils.singleLine("Enter first name", null, true);
        String lastName = shellUtils.singleLine("Enter last name", null, true);
        String middleName = shellUtils.singleLine("Enter middle name", null, false);
        String email = shellUtils.singleLine("Enter email address", null, true);
        data.put(ParamName.EMAIL, email);


        data.put(ParamName.FIRST_NAME, firstName);
        data.put(ParamName.LAST_NAME, lastName);
        data.put(ParamName.MIDDLE_NAME, middleName);

        boolean is_filer=shellUtils.promptWithOptions("Enable filer role?");
        boolean is_firm_admin=shellUtils.promptWithOptions("Enable firm admin role?");

        data.put(ParamName.IS_FILER, is_filer);
        data.put(ParamName.IS_FIRM_ADMIN, is_firm_admin);

        efsp.createFirmUser(data);
    }

    public void getFirmUser() throws Exception {
        isAuthenticated();
        Object[][] data = tableViewResolver.convertToShellTable(efsp.getFirmUser());
        shellHelper.table(data);
    }

    public void deleteFirmUser() throws Exception {
        isAuthenticated();
       String firmUserId=shellUtils.singleLine("Enter firm user id ",null,true);
       efsp.deleteFirmUser(firmUserId);
    }

}
