package com.ezcollab.cli.component;

import com.ezcollab.cli.http.ParamName;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.isAuthenticated;
import static com.ezcollab.cli.config.SH.*;

@SuppressWarnings("ALL")
@Component
public class AttorneyService {

    public String select() throws Exception {
        isAuthenticated();
        List<Map<String, String>> accountAccountList = efsp.attorneyAccounts();

        Map<String, String> accountAccountMap = new HashMap<>();
        for (Map<String, String> map : accountAccountList) {
            accountAccountMap.put(map.get("id"), map.get("display_name"));
        }
        String attorneyAccount = shellUtils.selectFromList(accountAccountMap, "Choose attorney account from list", "Select attorney account", true);
        return attorneyAccount;
    }

    public void list() throws Exception {
        isAuthenticated();
        List<Map<String, ?>> paymentAccountList = efsp.attorneyAccounts();
        Object[][] dataObject = tableViewResolver.convertToShellTable(paymentAccountList);
        shellHelper.printInfo("Attorney accounts list");
        shellHelper.table(dataObject);
    }


    public void create() throws Exception {
        isAuthenticated();
        String barnumber=shellUtils.singleLine("Enter your bar number ",null,true);
        String fname=shellUtils.singleLine("Enter your first name ",null,true);
        String lname=shellUtils.singleLine("Enter your last name ",null,true);
        String mname=shellUtils.singleLine("Enter your middle name ",null,false);

        Map data=new HashMap();
        data.put(ParamName.FIRST_NAME,fname);
        data.put(ParamName.LAST_NAME,lname);
        data.put(ParamName.MIDDLE_NAME,mname);
        data.put(ParamName.BAR_NUMBER,barnumber);
        efsp.createAttorneyUser(data);

        list();
    }

    public void delete() throws Exception {
        isAuthenticated();
        String attorneyId=select();
        efsp.deleteAttorneyUser(attorneyId);
    }

}
