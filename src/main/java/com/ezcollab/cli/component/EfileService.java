package com.ezcollab.cli.component;

import com.ezcollab.cli.config.LocalStorage;
import com.ezcollab.cli.http.ParamName;
import com.ezcollab.cli.http.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.*;
import static com.ezcollab.cli.config.SH.*;

@SuppressWarnings("ALL")
@Component
public class EfileService {


    public void processManual() throws Exception {
        LocalStorage.clearEfile();
        caseInfo();
        caseParties();
        filingDocuments();
        serviceContact();
        initEfile(ParamName.PAYMENT_ACCOUNT, paymentService.select());
        if (!LocalStorage.isIndividual()) {
            initEfile(ParamName.ATTORNEY, attorneyService.select());
        }
        crossReference();
        summary();
        submit();

    }

    public void summary() throws Exception {
        isAuthenticated();
        Response feeCalculation = efsp.feeCalculation();

        //display summary information
        Map<String, ?> feeCal = feeCalculation.getItem();

        //table creation work
        Object[][] feeCalData = new Object[feeCal.size()][];
        int count = 0;
        for (Map.Entry<String, ?> feeCalEntry : feeCal.entrySet()) {
            feeCalData[count] = new Object[]{feeCalEntry.getKey(), feeCalEntry.getValue()};
            count++;
        }
        shellHelper.printInfo("Fee Calculation : ");
        shellHelper.table(feeCalData);


    }

    public void submit() throws Exception {
        isAuthenticated();
        boolean submitFile = shellUtils.promptWithOptions("Do you want to file?");
        if (submitFile) {
            Response response;
            if (StringUtils.isBlank(getEnvelopId())) {
                response = efsp.submitFiling();
            } else {
                response = efsp.submitFiling(getEnvelopId());
            }
            shellHelper.printSuccess("Your filing has been submitted to the court.");
            Map<String, ?> item = response.getItem();

            String envelopdId = (String) item.get("envelope_id");
            List<Map<String, String>> filing = (List) item.get("filings");

            //table creation work
            Object[][] sampleData = new Object[filing.size() + 1][];
            sampleData[0] = new Object[]{"Status", "Filing Id", "Filing Code"};
            int count = 1;
            for (Map filingMap : filing) {
                sampleData[count] = new Object[]{filingMap.get("status"), filingMap.get("filing_id"), filingMap.get("filing_code")};
                count++;
            }
            shellHelper.printInfo("Envelopd Id : " + envelopdId);
            shellHelper.table(sampleData);

        }
//        shellHelper.printInfo("You have not submitted your filing yet if we want to file that envelop then use refile command until you create another filing or terminate shell.");
        shellHelper.printInfo("You have not submitted your filing.");
    }

    private void crossReference() throws Exception {
        //cross reference
        Map<String, String> crossReferenceOption = code.crossReferenceCode(true);
        if (!crossReferenceOption.isEmpty()) {
            String crossRefCode = shellUtils.selectFromList(crossReferenceOption, "Choose cross reference code", "Select cross reference code", true);
            String crossRefNumber = shellUtils.singleLine("Enter cross reference number", "Enter cross reference number", true);

            Map map = new HashMap();
            map.put(ParamName.CODE, crossRefCode);
            map.put(ParamName.NUMBER, crossRefNumber);
            initCrossRef(map);
        }
    }

    private void serviceContact() throws Exception {
        if (LocalStorage.isServiceContactEnable()) {
            String serviceContactId = serviceContactService.select();
            Map map = new HashMap();
            map.put("id", serviceContactId);
            initServiceContact(map);
        }


    }

    private void filingDocuments() throws Exception {
        //filing
        boolean filingContinue;
        do {
            Map<String, String> filingCodes = code.filingCode();
            String filingCode = shellUtils.selectFromList(filingCodes, "Choose filing code", "Select filing code", true);
            shellHelper.printInfo("Load " + filingCodes.get(filingCode) + " Information");
            boolean uploadContinue;
            Map documentType = code.documentType(filingCode);
            do {

                Map<String, Object> finalDoc = new HashMap<>();
                finalDoc.put(ParamName.CODE, filingCode);

                Map<String, String> filingDocuments = code.filingDocuemnt(filingCode);
                String filingDocument = shellUtils.selectFromList(filingDocuments, "Choose filing documents", "Select filing document", true);

                finalDoc.put(ParamName.FILING_DESCRIPTION, filingCodes.get(filingCode));
                finalDoc.put(ParamName.LEAD_DOC_CODE, filingDocument);
                String path = shellUtils.singleLine("Enter a full path of document(with / not \\)", null, true);
                String filename = StringUtils.substring(path, StringUtils.lastIndexOf(path, "/") + 1);
                initFile(path);

                finalDoc.put(ParamName.LEAD_DOC_PATH, filename);

                if (!documentType.isEmpty()) {
                    String leadDocSecurity = shellUtils.selectFromList(documentType, "Choose document security", "Select document security", true);
                    finalDoc.put(ParamName.LEAD_DOC_SECURITY, leadDocSecurity);
                }
                Map filingTypes = code.getFilingType(true);
                String filingType = shellUtils.selectFromList(filingTypes, "Choose filing types", "Select filing type", true);
                finalDoc.put(ParamName.FILING_TYPE, filingType);
                if (!StringUtils.equalsIgnoreCase(filingType, "efile")) {
                    LocalStorage.setServiceContactEnable(true);
                }
                initFiling(finalDoc);
                uploadContinue = shellUtils.promptWithOptions("Do you want to upload more file?");
            } while (uploadContinue);

            filingContinue = shellUtils.promptWithOptions("Do you want to file more?");
        } while (filingContinue);

    }

    private void caseParties() throws Exception {
        //case party information  case_party
        Map<String, String> partyTypeList = code.partyTypeCode(true);
        for (Map.Entry<String, String> entry : partyTypeList.entrySet()) {
            shellHelper.printInfo("Enter " + entry.getValue() + " Information");

            Map<String, Object> finalParty = new HashMap<>();
            boolean isBusiness = shellUtils.promptWithOptions("Is business party?");
            String businessName = "";
            String firstName = "";
            String middleName = "";
            String lastName = "";
            if (isBusiness) {
                businessName = shellUtils.singleLine("BusinessName", "business name is empty", true);
            } else {
                firstName = shellUtils.singleLine("First Name", "first name is empty", true);
                lastName = shellUtils.singleLine("Last Name", "last name is empty", true);
                middleName = shellUtils.singleLine("Middle Name", null, false);
            }
            finalParty.put(ParamName.TYPE, entry.getKey());
            finalParty.put(ParamName.BUSINESS_NAME, businessName);
            finalParty.put(ParamName.FIRST_NAME, firstName);
            finalParty.put(ParamName.LAST_NAME, lastName);
            finalParty.put(ParamName.MIDDLE_NAME, middleName);

            boolean isAddress = shellUtils.promptWithOptions("Is address available?");
            String city = "";
            String address1 = "";
            String state = "";
            String address2 = "";
            String zip = "";
            if (isAddress) {
                city = shellUtils.singleLine("City", null, true);
                address1 = shellUtils.singleLine("Address one ", null, true);
                state = shellUtils.singleLine("State", null, true);
                address2 = shellUtils.singleLine("Address Two", null, false);
                zip = shellUtils.singleLine("Zip", null, true);
            }
            finalParty.put(ParamName.ADDRESS_CITY, city);
            finalParty.put(ParamName.ADDRESS_ONE, address1);
            finalParty.put(ParamName.ADDRESS_STATE, state);
            finalParty.put(ParamName.ADDRESS_TWO, address2);
            finalParty.put(ParamName.ADDRESS_ZIP, zip);

            //check particular case party is filing party or not
            boolean isFilingParty = shellUtils.promptWithOptions("Is filing party?");
            if (isFilingParty) {
                finalParty.put(ParamName.IS_FILING_PARTY, "true");
            }


            finalParty.put(ParamName.PARTY_ID, "");
            initCaseParty(finalParty);
        }


    }

    private void damangeAmount() {
        Map caseCateogryCode = caseCategoryCode().get(getEfile(ParamName.CASE_CATEGORY));
        String damageAmount;
        String procedureRemedy;
        if (StringUtils.isBlank((CharSequence) caseCateogryCode.get("damage_amount_initial")) && StringUtils.isBlank((CharSequence) caseCateogryCode.get("damage_amount_subsequent")))
            return;
        if (isInitialCase()) {

        }else{

        }

    /*    initEfile(ParamName.DAMAGE_AMOUNT, efmCase.getDamageAmt());
        initEfile(ParamName.PROCEDURE_REMEDY, efmCase.getProcedureRemedy());*/

    }

    private void caseInfo() throws Exception {
        initEfile(ParamName.JURISDICTION, locationSelect());
        initEfile(ParamName.CASE_CATEGORY, shellUtils.selectFromList(code.caseCategory(), "Choose case category code", "Select case category code", true));
        initEfile(ParamName.CASE_TYPE, shellUtils.selectFromList(code.caseType(), "Choose case type code", "Select case type code", true));


        initEfile(ParamName.ECF_CASE_TYPE, null);
        initEfile(ParamName.FILING_PARTY_NAME, "");
        initEfile(ParamName.FILER_TYPE, "");

    }


    private String locationSelect() throws Exception {
        return shellUtils.selectFromList(code.locationCode(), "Choose location code", "Select location code", true);
    }

    public void filingStatus() throws Exception {
        isAuthenticated();
        String filingId = shellUtils.singleLine("Enter filing id", "Filing Id cannot be empty", true);
        Object[][] data = tableViewResolver.convertToShellTable(efsp.filingStatus(filingId));
        shellHelper.table(data);
    }

    public void cancelFiling() throws Exception {
        String filingId = shellUtils.singleLine("Enter filing id", "Filing Id cannot be empty", true);
        efsp.cancelFiling(filingId);

    }

    public String searchCase(String locationId, String caseNumber) throws Exception {
        isAuthenticated();
        String casetrackingid = efsp.searchCase(locationId, caseNumber);
        if(StringUtils.isBlank(casetrackingid)){
            throw new Exception("Case doesnot exist");
        }
        return casetrackingid;
    }

    public void searchCase() throws Exception {
        isAuthenticated();
        String caseNumber = shellUtils.singleLine("Enter case number", "Case number cannot be empty", true);
        searchCase(locationSelect(), caseNumber);
    }

    public void caseDetails() throws Exception {
        isAuthenticated();
        String caseTrackingId = shellUtils.singleLine("Enter case tracking id", "Case tracking id cannot be empty", true);
        caseDetails(caseTrackingId);
    }

    public void caseDetails(String caseTrackingId) throws Exception {
        isAuthenticated();
        Object[][] data = tableViewResolver.convertToShellTable(caseDetail(caseTrackingId));
        shellHelper.table(data);
    }

    public Map<String, ?> caseDetail(String caseTrackingId) throws Exception {
        return efsp.caseDetails(caseTrackingId);
    }

    public void filingList() throws Exception {
        isAuthenticated();
        Object[][] data = tableViewResolver.convertToShellTable(efsp.filingList());
        shellHelper.table(data);
    }


}
