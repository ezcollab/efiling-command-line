package com.ezcollab.cli.component;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ezcollab.cli.config.LocalStorage.isAuthenticated;
import static com.ezcollab.cli.config.SH.*;

@SuppressWarnings("ALL")
@Component
public class PaymentService {

    public String select() throws Exception {
        isAuthenticated();
        List<Map<String, ?>> paymentAccountList = efsp.paymentAccounts();
        Map<String, String> paymentAccountMap = new HashMap<>();
        for (Map<String, ?> map : paymentAccountList) {
            paymentAccountMap.put((String) map.get("payment_account_id"), (String) map.get("account_name"));
        }
        String paymentAccount = shellUtils.selectFromList(paymentAccountMap, "Choose payment account from list", "Select payment account", true);
        return paymentAccount;
    }


    public void list() throws Exception {
        isAuthenticated();
        List<Map<String, ?>> paymentAccountList = efsp.paymentAccounts();
        Object[][] dataObject = tableViewResolver.convertToShellTable(paymentAccountList);
        shellHelper.printInfo("Payment account list");
        shellHelper.table(dataObject);
    }

    public void create() throws Exception {
        isAuthenticated();
        shellHelper.printInfo("This functionality is not provided in this version.");
    }

    public void delete() throws Exception {
        isAuthenticated();
        shellHelper.printInfo("This functionality is not provided in this version.");
       /* String id = select();
        efsp.deleteServiceContactUser(id);*/
    }
}
