package com.ezcollab.cli.shell;

import com.ezcollab.cli.config.SH;
import org.apache.commons.lang3.StringUtils;
import org.jline.terminal.Terminal;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.table.ArrayTableModel;
import org.springframework.shell.table.BorderStyle;
import org.springframework.shell.table.TableBuilder;
import org.springframework.shell.table.TableModel;

import java.io.PrintWriter;

public class ShellHelper {

    @Value("${shell.out.info}")
    public String infoColor;

    @Value("${shell.out.success}")
    public String successColor;

    @Value("${shell.out.warning}")
    public String warningColor;

    @Value("${shell.out.error}")
    public String errorColor;

    private Terminal terminal;

    public ShellHelper(Terminal terminal) {
        this.terminal = terminal;
    }


    public String getColored(String message, PromptColor color) {
        return (new AttributedStringBuilder()).append(message, AttributedStyle.DEFAULT.foreground(color.toJlineAttributedStyle())).toAnsi();
    }

    public String getInfoMessage(String message) {
        return getColored(message, PromptColor.valueOf(infoColor));
    }

    public String getSuccessMessage(String message) {
        return getColored(message, PromptColor.valueOf(successColor));
    }

    public String getWarningMessage(String message) {
        return getColored(message, PromptColor.valueOf(warningColor));
    }

    public String getErrorMessage(String message) {
        return getColored(message, PromptColor.valueOf(errorColor));
    }


    public void print(String message) {
        print(message, null);
    }


    public void printSuccess(String message) {
        seperator();
        print(message, PromptColor.valueOf(successColor));
        seperator();
    }


    public void printInfo(String message) {
        seperator();
        print(message, PromptColor.valueOf(infoColor));
        seperator();
    }


    public void printWarning(String message) {
        seperator();
        print(message, PromptColor.valueOf(warningColor));
        seperator();
    }


    public void printError(String message) {
        seperator();
        print(message, PromptColor.valueOf(errorColor));
        seperator();
    }

    private void seperator(){
        print(getColored("  ",PromptColor.WHITE));
    }


    public void print(String message, PromptColor color) {
        String toPrint = message;
        if (color != null) {
            toPrint = getColored(message, color);
        }
        terminal.writer().println(toPrint);
        terminal.flush();
    }

    public void loading() {
        SH.shellHelper.print("Loading...", PromptColor.BRIGHT);
    }

    public void loading(String message) {
        SH.shellHelper.print("\t Loading :" + message + " ...............", PromptColor.BRIGHT);
    }

    public void description(String title, String message) {
        String wrapper="\t**************************************************************************";

        SH.shellHelper.print(wrapper, PromptColor.GREEN);
        SH.shellHelper.print("\t                    " + StringUtils.upperCase(title) + "                                 ", PromptColor.BRIGHT);
        if (StringUtils.isNotBlank(message)) {
            SH.shellHelper.print("\t Info : " + message, PromptColor.BRIGHT);
        }
        SH.shellHelper.print(wrapper, PromptColor.GREEN);

    }

    public void table(Object[][] tableData) {

        TableModel model = new ArrayTableModel(tableData);
        TableBuilder tableBuilder = new TableBuilder(model);
        tableBuilder.addFullBorder(BorderStyle.fancy_light);

        SH.shellHelper.print(tableBuilder.build().render(50));
    }

    //--- set / get methods ---------------------------------------------------

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }
}
