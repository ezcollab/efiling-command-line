package com.ezcollab.cli.utils;

import com.ezcollab.cli.config.SH;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Map;

@Component
public class ShellUtils {


    public String singleLine(String enterMessage, String errorMessage, boolean isRequired) {
        if (org.apache.commons.lang3.StringUtils.isBlank(errorMessage) || org.apache.commons.lang3.StringUtils.equalsIgnoreCase("error", errorMessage)) {
            errorMessage = "field is required";
        }
        String resultHolder = null;
        do {
            String fullName = SH.inputReader.prompt(enterMessage);
            if (StringUtils.hasText(fullName)) {
                resultHolder = fullName;
            } else if (isRequired) {
                SH.shellHelper.printWarning(errorMessage);
            }
        } while (org.apache.commons.lang3.StringUtils.isBlank(resultHolder) && isRequired);
        return resultHolder;

    }

    /**
     * key will use as code
     * value as display name
     */
    public String selectFromList(Map<String, String> options, String headingMessage, String promptMessage, boolean isRequired) throws Exception {
        if (!org.apache.commons.lang3.StringUtils.endsWith(headingMessage, ":")) {
            headingMessage += " : ";
        }
        String resultHolder = SH.inputReader.selectFromList(headingMessage, promptMessage, options, true, null, isRequired);
        return resultHolder;
    }

    public boolean promptWithOptions(String message) throws Exception {
        String resultHolder = SH.inputReader.promptWithOptions(message, "N", Arrays.asList("Y", "N"));
        return org.apache.commons.lang3.StringUtils.equalsIgnoreCase("Y", resultHolder);
    }

    public void checkTerminationPoint(String message) throws Exception {
        if (org.apache.commons.lang3.StringUtils.equalsIgnoreCase(message, "/q")) {
            throw new Exception("terminated process");
        }
    }

}
