package com.ezcollab.cli.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TableViewResolver {

    public Object[][] convertToShellTable(Map<String, ?> data) {
        Object[][] dataObject = new Object[data.size()][];
        int count = 0;
        for (Map.Entry<String, ?> entry : data.entrySet()) {
            dataObject[count] = new Object[]{headingCreator(entry.getKey()), entry.getValue()};
            count++;
        }

        return dataObject;
    }


    public Object[][] convertToShellTable(List<Map<String, ?>> data) {
        Object[] headersObject = null;
        for (Map<String, ?> map : data) {
            headersObject = new Object[map.size()];
            int count = 0;
            for (Map.Entry<String, ?> entry : map.entrySet()) {
                headersObject[count] = headingCreator(entry.getKey());
                count++;
            }
            break;
        }
        Object[][] dataObject = new Object[data.size() + 1][];
        dataObject[0] = headersObject;
        int parentIndex = 1;
        for (Map<String, ?> map : data) {
            int count = 0;
            Object[] childObject = new Object[map.size()];
            for (Map.Entry<String, ?> entry : map.entrySet()) {
                childObject[count] = entry.getValue();
                count++;
            }
            dataObject[parentIndex] = childObject;
            parentIndex++;
        }
        return dataObject;

    }

    private String headingCreator(String header) {
        return StringUtils.upperCase(StringUtils.replace(header, "_", " "));
    }


}
