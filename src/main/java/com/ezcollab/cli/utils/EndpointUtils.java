package com.ezcollab.cli.utils;

import com.ezcollab.cli.config.LocalStorage;
import com.ezcollab.cli.config.SH;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class EndpointUtils {


    public static String getEndpoint(String key) throws Exception {
        String state=LocalStorage.getState();
        String endpoint= SH.env.getProperty(state+"."+key);
        if(StringUtils.isBlank(endpoint)){
            throw new Exception("endpoint not exist");
        }
        return endpoint;
    }

}
