package com.ezcollab.cli;

import com.ezcollab.cli.config.SH;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalException {

    @ExceptionHandler({Exception.class,RuntimeException.class})
    public void ex(Exception x){
        SH.shellHelper.printError(x.getMessage());
    }

}
